SPACE INVADERS
==============

The game is realized with ncurses library. Is an embryonic version of final game.
To play, open a shell on the directory "src" and type the command "make run".
This command compiles the game and launch it.
Then, when game was launched, type s to start and q to quit. Option "h" has not been yet realized.

THE REPOSITORY HAS BEEN MOVED TO GITHUB AT LINK